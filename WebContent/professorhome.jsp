<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Professor</title>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="qrcode.js"></script>
<style type="text/css">
	.logout
		{
			position:relative;
			left: 1200px;
			top:20px;
		}
</style>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">                   
      	<span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
      <a class="navbar-brand" href="#"><Strong>Northwest Online Attendance</Strong></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="Login.jsp"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
</head>
<body>
<a class="logout" href="Login.jsp" style="padding:left"><button>Logout</button></a>
<h1>professor module</h1>
<h3>Welcome <b><%= request.getParameter("username")%></b></h3>
</body>

<!-- <input id="text" type="text"  style="width:25%" /><br /> -->
<input id="text" type="text"  value="GDPII" style="width:25%" readonly="readonly" /><button id="click">Click</button><br><br>
<input id="text" type="text"  value="Web Apps" style="width:25%" readonly="readonly" /><button id="click">Click</button>
<div id="qrcode" style="width:300px; height:300px; margin-top:15px;"></div>

<script type="text/javascript">
$('#click').click(function(){
$('#qrcode').html('');
function makeCode () {	
	
	var qrcode = new QRCode(document.getElementById("qrcode"), {
		width : 300,
		height : 300
	}); 
	var today = new Date();
	var dd = today.getDate();

	var mm = today.getMonth()+1; 
	var yyyy = today.getFullYear();
	if(dd<10) 
	{
	    dd='0'+dd;
	} 

	if(mm<10) 
	{
	    mm='0'+mm;
	} 
	today = mm+'/'+dd+'/'+yyyy;
	console.log(today);
	var time = new Date().toTimeString().split(" ")[0];
	var elText = document.getElementById("text");
	console.log(elText);
	qrcode.makeCode(elText.value+today+time);
	
}
makeCode();	
}); 
</script>
</body>
</html>