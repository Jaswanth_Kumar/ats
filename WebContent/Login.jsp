<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
<h1>Attendance Tracking System</h1>
<style type="text/css">
	body {
    background:#333;
}
.form_bg {
    background-color:#eee;
    color:#666;
    padding:20px;
    border-radius:10px;
    position: absolute;
    border:1px solid #fff;
    margin: auto;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 320px;
    height: 280px;
}
.align-center {
    text-align:center;
}
.align-left{
	text-align:left;
}
.align-right{
	text-align:right;
}
h2
{
	color:yellow;
	font-family: verdana;
}
.text-center
{
	font-weight: bold;
	color: green;
}
#bg {
  position: fixed; 
  top: -50%; 
  left: -50%; 
  width: 200%; 
  height: 200%;
}
#bg img {
  position: absolute; 
  top: 0; 
  left: 0; 
  right: 0; 
  bottom: 0; 
  margin: auto; 
  min-width: 50%;
  min-height: 50%;
}
</style>
</head>
<body>


	<div id="bg">
  		<img src="http://www.cmcadamsphotography.com/img/s/v-3/p1127381168-3.jpg" alt="">
	</div>
	<div class="container">
    <div class="row">
        <div class="form_bg">
            
            <form name="loginform" action="LoginServlet" method="post">
                 <h2 class="text-center">Login</h2>
                <br/>
                <div class="form-group">
                    <input type="text" name="username" class="form-control" id="username" placeholder="Username" required="required">
                    <p id="text"></p>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" id="pwd" placeholder="password" required="required">
                </div>
                <br/>
                <div class="align-left">
                    <button type="submit" class="btn btn-primary  btn-block" id="login">Login</button>
                </div>
             <!--   <div class="align-right">
                	<button type="reset" class="btn btn-danger btn-block" value="Reset">Reset</button>
                </div>  -->
            </form>
        </div>
    </div>
    
</div>
</body>
</html>


























