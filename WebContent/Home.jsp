<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome to ATS</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<style type="text/css">
	.Subjects
		{
			position: absolute;
  			width: 300px;
  			height: 200px;
		    z-index: 0;
  			top: 50%;
  			left: 50%;
  			margin: -100px 0 0 -150px;
		}
	.welcome
		{
			left:99px;
			right:50px;
			position:absolute;
			z-index: 0;	
			font-family: verdana;
		}
	.logout
		{
			position:relative;
			left: 1200px;
			top:20px;
		}
</style>
</head>
<body>

	
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">                   
      	<span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
      <a class="navbar-brand" href="#"><Strong>Northwest Online Attendance</Strong></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="Login.jsp"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
	
	<h2 style = "text-align:center"><u><%= request.getParameter("username") %></u> &nbsp <u> Attendance</u></h2><br>
	<div class="welcome">
	</div>
	<div class="Subjects" style = "align:center">
		<h3><label class="label label-default">HCI</label><label class="label label-success">95%</label></h3>
		<h3><label class="label label-default">GDP II</label><label class="label label-success">98%</label></h3>
		<h3><label class="label label-default">C# .Net</label><label class="label label-success">95%</label></h3>
	</div>
</body>
</html>