package com.ats.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.ats.bean.LoginBean;
import com.ats.bean.subjectbean;
import com.ats.util.DBConnection;

public class SubjectDao {
	
	public static List<subjectbean> getsubjects(LoginBean loginbean) throws ClassNotFoundException{
		String userName = loginbean.getUsername();
		System.out.println(userName);
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List <subjectbean> subjectlist = new ArrayList<>();
		try{
			
			con = DBConnection.createconnection();
			st = con.createStatement();
			rs = st.executeQuery("select subject_name from student_courses,students where students.sid = '"+userName+"' "
					+ "and student_courses.subject_id = students.subject_id");
			System.out.println(rs.next());
			
			while(rs.next())
			{
				subjectbean sb = new subjectbean();
				sb.subjectlist.add(rs.getString(0));
				subjectlist.add(sb);
			}
			
		}
		catch (SQLException e) {
			// TODO: handle exception
		}
		return subjectlist;
		
	}

}
