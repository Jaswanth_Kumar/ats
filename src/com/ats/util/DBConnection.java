package com.ats.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnection {

	public static Connection createconnection() throws SQLException
	{
		Connection con = null;
		String url = "jdbc:mysql://127.0.0.1:3306/ats";
		String username = "root";
		String password = "1234";
		try 
		{
				try 
				{
					Class.forName("com.mysql.jdbc.Driver"); //loading mysql driver 
					
				} 
				catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				} 
			con = DriverManager.getConnection(url, username, password); //attempting to connect to MySQL database
			
		} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		return con; 
		}
}
