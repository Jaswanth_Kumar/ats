package com.ats.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ats.bean.LoginBean;
import com.ats.bean.subjectbean;
import com.ats.dao.LoginDao;
import com.ats.dao.SubjectDao;


public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;
       
   
    public LoginServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		LoginBean loginbean = new LoginBean();
		loginbean.setUsername(userName);
		loginbean.setPassword(password);
		LoginDao logindao = new LoginDao();
		String uservalidate = logindao.authenticateuser(loginbean);
		
		try {
			List<subjectbean> subjectlist =  SubjectDao.getsubjects(loginbean) ;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();}
		if(uservalidate.equals("Student"))
		{
			request.setAttribute("userName", userName);
			request.getRequestDispatcher("/Home.jsp").forward(request, response);
		}
		else if(uservalidate.equals("professor"))
		{
			request.setAttribute("userName", userName);
			request.getRequestDispatcher("/professorhome.jsp").forward(request, response);
		}
		else
		{
			request.setAttribute("errMessage", uservalidate);
			request.getRequestDispatcher("/Login.jsp").forward(request, response);
		}
		
	}
	

}
